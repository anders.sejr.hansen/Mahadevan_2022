Code for FRAP and SPT analysis supporting Mahadevan et al. 2022
--------------------------

# Overview
This repository contains Matlab code for analysis of *Single-Particle
Tracking* (SPT) and *Fluorescence Recovery After Photobleaching* (FRAP) data.

The subfolder `FRAP` contains all the FRAP code together with a brief
guide. The FRAP code was originally used and reported in:

    CTCF and cohesin regulate chromatin loop stability with distinct dynamics

    Hansen AS, Pustova I, Cattoglio C, Tjian R, Darzacq X. elife. 2017 May 3;6:e25776.

The subfolder `SPT` contains the SPT analysis code and is very
slightly modified from the existing GitLab repository
[SPT_LocAndTrac](https://gitlab.com/tjian-darzacq-lab/SPT_LocAndTrack). 


# How to cite

If you use this code please cite:

    {insert citation when published}

# License
These scripts are all are released under the GNU General Public License version 3 or upper (GPLv3+).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
