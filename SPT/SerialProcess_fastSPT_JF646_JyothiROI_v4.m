%   SerialProcess_fastSPT_JF646_JyothiROI.m
%   Anders Sejr Hansen, August 2020
clear; clc; close all; clearvars -global

%   DESCRIPTION
%   This script takes as input a folder with nd2 files and then outputs
%   workspaces with tracked single molecules. Briefly, it uses the
%   BioFormats package to read in nd2 files. Please double-check that this
%   does not change the pixel intensity values. Nikon keeps updating the
%   nd2 file format, so if you are using mis-matched Nikon Elements
%   software versions and bioformat versions, this is a major issue that
%   you should be aware of. Next, the script feeds the images as a 3D
%   matrix into the localization part of the MTT algorithm (Part 1) and
%   subsequently, the tracked particles are fed into the tracking part of
%   the MTT algorithm (Part 2). 

%   JYOTHI V2
%   Features of the V2 script
%   1. divide trajectories into inside and outside the ROI
%   2. Make "control" ROIs above and below the 405-ROI
%   3. Temporoal Binning of trajectories



%%%%%%%%%%%%%%%%%%%% DEFINE INPUT AND OUTPUT PATHS %%%%%%%%%%%%%%%%%%%%%%%%
% specify input path with nd2 files:
input_path=([pwd, filesep, 'SPT_Data', filesep, 'More_data_PARP1_Damage', filesep]);
output_path=([pwd, filesep, 'Output', filesep, 'More_data_PARP1_Damage', filesep]);
%output_path = input_path;
%%%%% make output folder if it does not already exist
if exist(output_path) == 7
    % OK, output_path exists and is a directory (== 7). 
    disp('The given output folder exists. MATLAB workspaces will be save to:');
    disp(output_path);
else
    mkdir(output_path);
    disp('The given output folder did not exist, but was just created. MATLAB workspaces will be save to:');
    disp(output_path);
end

LocalizationError = -6.25; % Localization Error: -6 = 10^-6
EmissionWavelength = 664; % wavelength in nm; consider emission max and filter cutoff
ExposureTime = 10.3; % in milliseconds
NumDeflationLoops = 0; % Generaly keep this to 0; if you need deflation loops, you are imaging at too high a density;
MaxExpectedD = 6; % The maximal expected diffusion constant for tracking in units of um^2/s;
NumGapsAllowed = 1; % the number of gaps allowed in trajectories
FrameToStart = 20; % all frames below this number will be ignored and all trajectories starting before this frame will be removed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%% JYOTHI PARAMETERS
% Define how many micrometers above and below the ROI the reference/control
% ROI should be:
control_ROI = 3; %number of um above and below for control ROI
% Define how many temporal bins you would to divide the trajectories into
temporal_bins = 4; % number of time-bins to divide trajs into
ROI_expand = 2; % if ROI_expand = 0, then there will be no expansion and the provide ROI will be used;
% if ROI_expand >0, then there will be uniform padding to expand the ROI.
% E.g. if ROI_expand = 3, then 3 additional pixels will be added to the
% top, bottom, left, and right of the ROI to uniformly expand it. 



%%% DEFINE STRUCTURED ARRAY WITH ALL THE SPECIFIC SETTINGS FOR LOC AND TRACK
% imaging parameters
impars.PixelSize=0.16; % um per pixel
impars.psf_scale=1.35; % PSF scaling
impars.wvlnth= EmissionWavelength/1000; %emission wavelength in um
impars.NA=1.49; % NA of detection objective
impars.psfStd= impars.psf_scale*0.55*(impars.wvlnth)/impars.NA/1.17/impars.PixelSize/2; % PSF standard deviation in pixels
impars.FrameRate= ExposureTime/1000; %secs
impars.FrameSize= ExposureTime/1000; %secs

% localization parameters
locpars.wn=9; %detection box in pixels
locpars.errorRate= LocalizationError; % error rate (10^-)
locpars.dfltnLoops= NumDeflationLoops; % number of deflation loops
locpars.minInt=0; %minimum intensity in counts
locpars.maxOptimIter= 50; % max number of iterations
locpars.termTol= -2; % termination tolerance
locpars.isRadiusTol=false; % use radius tolerance
locpars.radiusTol=50; % radius tolerance in percent
locpars.posTol= 1.5;%max position refinement
locpars.optim = [locpars.maxOptimIter,locpars.termTol,locpars.isRadiusTol,locpars.radiusTol,locpars.posTol];
locpars.isThreshLocPrec = false;
locpars.minLoc = 0;
locpars.maxLoc = inf;
locpars.isThreshSNR = false;
locpars.minSNR = 0;
locpars.maxSNR = inf;
locpars.isThreshDensity = false;

% tracking parameters
trackpars.trackStart=1;
trackpars.trackEnd=inf;
trackpars.Dmax= MaxExpectedD;
trackpars.searchExpFac=1.2;
trackpars.statWin=10;
trackpars.maxComp=5;
trackpars.maxOffTime=NumGapsAllowed;
trackpars.intLawWeight=0.9;
trackpars.diffLawWeight=0.5;


% add the required functions to the path:
addpath([pwd, filesep, 'SLIMFAST_batch_fordist']);
addpath([pwd, filesep, 'SLIMFAST_batch_fordist', filesep, 'bfmatlab']);
disp('added paths for MTT algorithm mechanics, bioformats...');

%%%%%%%%%%%%%% READ IN ND2 FILES AND CONVERT TO TIFF FILES %%%%%%%%%%%%%%%%
%disp('-----------------------------------------------------------------');
%disp('reading in nd2 files; writing out MAT workspaces...');
%find all nd2 files:
nd2_files=dir([input_path,'*.nd2']);
Filenames = ''; %for saving the actual file name

for iter = 1:length(nd2_files)
    Filenames{iter} = nd2_files(iter).name(1:end-4);
end
% read in the nd2 file using BioFormats and save a tiff:
for iter = 1:length(Filenames)
    disp('-----------------------------------------------------------------');
    tic; 
    disp(['reading in nd2 file ', num2str(iter), ' of ', num2str(length(nd2_files)), ' total nd2 files']);
    %%% read nd2 files:
    img_stack_cell_array = bfopen([input_path, Filenames{iter}, '.nd2']);
    cell_array_2d = img_stack_cell_array{1}; %find pixel vals
    imgs_only_cell_array = cell_array_2d(:,1); % get rid of excess info
    imgs_3d_matrix = cat(3, imgs_only_cell_array{:}); % 3d image matrix
    %convert to double, re-scale to 16 bit:
    imgs_3d_double = (2^16-1)*im2double(imgs_3d_matrix);
    number_of_frames = size(imgs_only_cell_array,1);
    
    toc;
    clear img_stack_cell_array cell_array_2d imgs_only_cell_array 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%% MTT ALGORITHM PART 1: LOCALIZE ALL PARTICLES %%%%%%%%%%%%%%
    disp('MTT ALGORITHM PART 1: localize particles in all of the workspaces');
    tic; 
    disp(['localizing all particles in movie number ', num2str(iter), ' of ', num2str(length(Filenames))]);
    impars.name = Filenames{iter};
    data = localizeParticles_ASH(input_path,impars, locpars, imgs_3d_matrix);
    %data = localizeParticles_ASH(input_path,impars, locpars, imgs_3d_double);
    toc;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%% MTT ALGORITHM PART 2: TRACK PARTICLES BETWEEN FRAMES %%%%%%%%%
    disp('MTT ALGORITHM PART 2: track particles between frames');
    tic; 
    disp(['tracking all localized particles from movie ', num2str(iter), ' of ', num2str(length(Filenames))]);
    data=buildTracks2_ASH(input_path, data,impars, locpars, trackpars, data.ctrsN, imgs_3d_double);
    toc;
    
    %%%%%%%% LOAD IN THE CORRESPONDING ROI %%%%%%
    tic;
    disp('reading in the TIFF ROI file');
    read_ROI_img = imread([input_path, Filenames{iter}, '_ROI.tif']);
    if ROI_expand > 0
        strel = ones(round(ROI_expand)+1, round(ROI_expand)+1); %expand the ROI
        ROI_img = imdilate(read_ROI_img,strel);
    else
        ROI_img = read_ROI_img;
    end
    toc;
    
    %%%%%%%% SAVE THE TRAJECTORIES TO YOUR STRUCTURED ARRAY FORMAT %%%%%%%%
    tic;
    disp('filter localization based on whether or not they are inside the ROI');
    data_cell_array = data.tr;
    % save meta-data
    settings.Delay = impars.FrameRate;
    settings.px2micron = impars.PixelSize;
    settings.TrackingOptions = trackpars;
    settings.LocOptions = locpars;
    settings.AcquisitionOptions = impars;
    settings.Filename = impars.name;
    settings.Width = size(imgs_3d_matrix,2);
    settings.Height = size(imgs_3d_matrix,1);
    settings.Frames = size(imgs_3d_matrix,3);
    All_trackedPar = struct;
    in_ROI_trackedPar = struct;
    out_ROI_trackedPar = struct;
    in_iter = 1; out_iter = 1; all_iter = 1;
    for i=1:length(data_cell_array)
        % first determine if each localization is inside or outside the ROI:
        temp_xy = data_cell_array{i}(:,1:2); %pixel units
        in_ROI = zeros(size(temp_xy,1),1);
        for frame_iter = 1:length(in_ROI)
            in_ROI(frame_iter, 1) = ROI_img(round(temp_xy(frame_iter, 2)), round(temp_xy(frame_iter, 1)));
        end 
        % save the key variables and convert to um
        temp_xy_um = impars.PixelSize .* temp_xy;
        Frame = data_cell_array{i}(:,3);
        TimeStamp = impars.FrameRate.* data_cell_array{i}(:,3);
        
        % We want to ignore the first part of the movie and only count
        % trajectories that begin after Frame# "FrameToStart"
        if round(min(Frame)) >= FrameToStart
        
            % save all trajectories:
            All_trackedPar(all_iter).xy = temp_xy_um;
            All_trackedPar(all_iter).Frame = Frame;
            All_trackedPar(all_iter).TimeStamp = TimeStamp;
            All_trackedPar(all_iter).in_ROI = in_ROI;
            all_iter = all_iter +1;
            
            % determine if at least one localization was inside the ROI:
            if sum(in_ROI) > 0
                % at least one localization was inside the ROI
                in_ROI_trackedPar(in_iter).xy = temp_xy_um;
                in_ROI_trackedPar(in_iter).Frame = Frame;
                in_ROI_trackedPar(in_iter).TimeStamp = TimeStamp;
                in_ROI_trackedPar(in_iter).in_ROI = in_ROI;
                in_iter = in_iter + 1;
            elseif sum(in_ROI) < 1
                % all localizations were outside the ROI
                out_ROI_trackedPar(out_iter).xy = temp_xy_um;
                out_ROI_trackedPar(out_iter).Frame = Frame;
                out_ROI_trackedPar(out_iter).TimeStamp = TimeStamp;
                out_ROI_trackedPar(out_iter).in_ROI = in_ROI;
                out_iter = out_iter + 1;
            end    
        end
    end
    disp(['Localized and tracked ', num2str(length(All_trackedPar)), ' trajectories']);
    disp([num2str(length(in_ROI_trackedPar)), ' trajectories were inside the ROI with at least one localization']);
    disp([num2str(length(out_ROI_trackedPar)), ' trajectories were entirely outside the ROI']);
    
    % generate images of the ROI and the localizations overlaid:
    all_img = zeros(size(ROI_img,1), size(ROI_img,2));
    in_img = zeros(size(ROI_img,1), size(ROI_img,2));
    out_img = zeros(size(ROI_img,1), size(ROI_img,2));
    %loop over all localizations and generate images
    for traj_iter = 1:length(All_trackedPar)
        curr_xy = round(All_trackedPar(traj_iter).xy./impars.PixelSize);
        curr_in_ROI = All_trackedPar(traj_iter).in_ROI;
        for frame_iter = 1:length(curr_in_ROI)
            all_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) = all_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) + 1;
            if curr_in_ROI(frame_iter) == 1
                % Particle is inside the ROI
                in_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) = all_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) + 1;
            else
                % it was outside the ROI
                out_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) = all_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) + 1;
            end            
        end
    end
    
    figure1 = figure('position',[10 10 800 900]); %[x y width height]
    
    % ROI image
    subplot(2,2,1);
    imshow(ROI_img, []);
    title(['ROI provided for ', Filenames{iter}], 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');    
    
    % generate perimeter
    bw_perim = bwperim(ROI_img);
    
    % overlay on all localizations
    subplot(2,2,2);
    overlay_all = imoverlay(all_img, bw_perim, [.3 1 .3]);
    imshow(overlay_all);
    title('All localizations', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');
    
    % overlay on all localizations
    subplot(2,2,3);
    overlay_in = imoverlay(in_img, bw_perim, [.3 1 .3]);
    imshow(overlay_in);
    title('Localizations only inside ROI', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');
    
    % overlay on all localizations
    subplot(2,2,4);
    overlay_out = imoverlay(out_img, bw_perim, [.3 1 .3]);
    imshow(overlay_out);
    title('Localizations only outside ROI', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');
    set(figure1,'Units','Inches');
    pos = get(figure1,'Position');
    set(figure1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
    print(figure1,[output_path, Filenames{iter}, '_RawLocs_ROI.pdf'],'-dpdf','-r0');
    
    %%%% NOW SAVE A MATLAB WORKSPACE FOR ALL, IN, and OUT TRAJCTORIES %%%% 
    disp(['saving MATLAB workspace for movie ', num2str(iter), ' of ', num2str(length(Filenames))]);
    
    disp('save all trajectories');
    clear trackedPar
    trackedPar = All_trackedPar;    
    save([output_path, Filenames{iter}, '_Tracked_AllTrajs.mat'], 'trackedPar', 'settings');
    
    disp('save trajectories with at least one Loc inside the ROI');
    clear trackedPar
    trackedPar = in_ROI_trackedPar;    
    save([output_path, Filenames{iter}, '_Tracked_In_ROI.mat'], 'trackedPar', 'settings');
    
    disp('save trajectories that were entirely outside thr ROI');
    clear trackedPar
    trackedPar = out_ROI_trackedPar;    
    save([output_path, Filenames{iter}, '_Tracked_outside_ROI.mat'], 'trackedPar', 'settings');
       
    toc;
    
    
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%% COLLECT TRAJECTORIES IN REFERENCE ROIs %%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tic; disp('Now find trajectories in the control/reference ROIs');
    
    % make the above control ROI. 
    Above_ROI_img = logical(zeros(size(ROI_img,1), size(ROI_img,2)));
    Below_ROI_img = logical(zeros(size(ROI_img,1), size(ROI_img,2)));
    
    % Number of pixels to shift:
    shift_integer = round(control_ROI/impars.PixelSize);
    
    % now find the corners of the ROI:
    ROI_corners = corner(ROI_img);
    Col_range = ROI_corners(1,2):ROI_corners(2,2);
    Row_range = ROI_corners(2,1):ROI_corners(3,1);
    
    %make above ROI:
    above_Col_range = Col_range - shift_integer;
    below_Col_range = Col_range + shift_integer;
    
    %make above and below logical images
    for col_iter = min(above_Col_range):max(above_Col_range)
        for row_iter = min(Row_range):max(Row_range)
            Above_ROI_img(col_iter,row_iter) = 1;
        end
    end
    for col_iter = min(below_Col_range):max(below_Col_range)
        for row_iter = min(Row_range):max(Row_range)
            Below_ROI_img(col_iter,row_iter) = 1;
        end
    end
    
    %NOW DIVIDE THE TRAJECTORIES INTO ABOVE AND BELOW:
    above_ROI_trackedPar = struct;
    below_ROI_trackedPar = struct;
    above_iter = 1; below_iter = 1; all_iter = 1;
    for i=1:length(data_cell_array)
        % first determine if each localization is inside or outside the ROI:
        temp_xy = data_cell_array{i}(:,1:2); %pixel units
        above_ROI = zeros(size(temp_xy,1),1);
        below_ROI = zeros(size(temp_xy,1),1);
        for frame_iter = 1:length(above_ROI)
            above_ROI(frame_iter, 1) = Above_ROI_img(round(temp_xy(frame_iter, 2)), round(temp_xy(frame_iter, 1)));
            below_ROI(frame_iter, 1) = Below_ROI_img(round(temp_xy(frame_iter, 2)), round(temp_xy(frame_iter, 1)));
        end 
        % save the key variables and convert to um
        temp_xy_um = impars.PixelSize .* temp_xy;
        Frame = data_cell_array{i}(:,3);
        TimeStamp = impars.FrameRate.* data_cell_array{i}(:,3);
        
        % We want to ignore the first part of the movie and only count
        % trajectories that begin after Frame# "FrameToStart"
        if round(min(Frame)) >= FrameToStart
        
            % save all trajectories:
            All_trackedPar(all_iter).xy = temp_xy_um;
            All_trackedPar(all_iter).Frame = Frame;
            All_trackedPar(all_iter).TimeStamp = TimeStamp;
            All_trackedPar(all_iter).in_ROI = in_ROI;
            all_iter = all_iter +1;        
               
            % determine if at least one localization was inside the above ROI:
            if sum(above_ROI) > 0
                % at least one localization was inside the ROI
                above_ROI_trackedPar(above_iter).xy = temp_xy_um;
                above_ROI_trackedPar(above_iter).Frame = Frame;
                above_ROI_trackedPar(above_iter).TimeStamp = TimeStamp;
                above_ROI_trackedPar(above_iter).above_ROI = above_ROI;
                above_iter = above_iter + 1;
            end    
            % determine if at least one localization was inside the below ROI:
            if sum(below_ROI) > 0
                % at least one localization was inside the ROI
                below_ROI_trackedPar(below_iter).xy = temp_xy_um;
                below_ROI_trackedPar(below_iter).Frame = Frame;
                below_ROI_trackedPar(below_iter).TimeStamp = TimeStamp;
                below_ROI_trackedPar(below_iter).below_ROI = below_ROI;
                below_iter = below_iter + 1;
            end 
        end
    end
    disp(['Localized and tracked ', num2str(length(All_trackedPar)), ' trajectories']);
    disp([num2str(length(above_ROI_trackedPar)), ' trajectories were in the above-ROI control ROI with at least one localization']);
    disp([num2str(length(below_ROI_trackedPar)), ' trajectories were in the below-ROI control ROI with at least one localization']);
    
    % generate images of the above and below ROI and the localizations overlaid:
    above_img = zeros(size(ROI_img,1), size(ROI_img,2));
    below_img = zeros(size(ROI_img,1), size(ROI_img,2));
    %loop over all localizations and generate images
    for traj_iter = 1:length(above_ROI_trackedPar)
        curr_xy = round(above_ROI_trackedPar(traj_iter).xy./impars.PixelSize);
        curr_above_ROI = above_ROI_trackedPar(traj_iter).above_ROI;
        for frame_iter = 1:length(curr_above_ROI)
            if curr_above_ROI(frame_iter) == 1
                % Particle is inside the ROI
                above_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) = above_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) + 1;   
            end
        end
    end
    for traj_iter = 1:length(below_ROI_trackedPar)
        curr_xy = round(below_ROI_trackedPar(traj_iter).xy./impars.PixelSize);
        curr_below_ROI = below_ROI_trackedPar(traj_iter).below_ROI;
        for frame_iter = 1:length(curr_below_ROI)
            if curr_below_ROI(frame_iter) == 1
                % Particle is inside the ROI
                below_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) = below_img(curr_xy(frame_iter, 2), curr_xy(frame_iter, 1)) + 1;   
            end
        end
    end
    
    
    figure2 = figure('position',[10 10 800 900]); %[x y width height]
    
    % ROI image
    % DO ABOVE ROI FIRST
    % generate perimeter
    bw_perim = bwperim(Above_ROI_img);
    
    subplot(2,2,1);
    overlay_in = imoverlay(above_img, bw_perim, [.3 1 .3]);
    imshow(overlay_in);
    title('Localizations only inside above-control-ROI', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');     
    
    % overlay on all localizations
    subplot(2,2,2);
    overlay_all = imoverlay(all_img, bw_perim, [.3 1 .3]);
    imshow(overlay_all);
    title('All localizations; box for above-control-ROI', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');
    
    % DO BELOW ROI FIRST
    % generate perimeter
    bw_perim = bwperim(Below_ROI_img);
    
    subplot(2,2,3);
    overlay_in = imoverlay(below_img, bw_perim, [.3 1 .3]);
    imshow(overlay_in);
    title('Localizations only inside below-control-ROI', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');     
    
    % overlay on all localizations
    subplot(2,2,4);
    overlay_all = imoverlay(all_img, bw_perim, [.3 1 .3]);
    imshow(overlay_all);
    title('All localizations; box for above-control-ROI', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k', 'Interpreter', 'none');
    
    set(figure2,'Units','Inches');
    pos = get(figure2,'Position');
    set(figure2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
    print(figure2,[output_path, Filenames{iter}, '_ControlROIs_above_below.pdf'],'-dpdf','-r0');
    
    
    %%%% NOW SAVE A MATLAB WORKSPACE FOR ABOVE-ROI and BELOW-ROI CONTROL TRAJCTORIES %%%% 
    disp(['saving MATLAB ABOVE-ROI and BELOW-ROI control trajectories for movie ', num2str(iter), ' of ', num2str(length(Filenames))]);
    
    
    disp('save trajectories with at least one Loc inside the Above-ROI control');
    clear trackedPar
    trackedPar = above_ROI_trackedPar;    
    save([output_path, Filenames{iter}, '_Tracked_In_Above-ROI.mat'], 'trackedPar', 'settings');
    
    disp('save trajectories with at least one Loc inside the Below-ROI control');
    clear trackedPar
    trackedPar = below_ROI_trackedPar;    
    save([output_path, Filenames{iter}, '_Tracked_In_Below-ROI.mat'], 'trackedPar', 'settings');
       
    toc;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%% DO TEMPORAL BINNING %%%%%%%%%%%%%%%%%%%%%%%%%%%
    tic;
    time_output_path = [output_path, 'TimeBins', filesep];
    %%%%% make output folder if it does not already exist
    if exist(time_output_path) == 7
        % OK, output_path exists and is a directory (== 7). 
        disp('The given output folder for TimeBins exists. MATLAB workspaces will be save to:');
        disp(time_output_path);
    else
        mkdir(time_output_path);
        disp('The given output folder for TimeBins did not exist, but was just created. MATLAB workspaces will be save to:');
        disp(time_output_path);
    end
    
    % divide all of the trajectories into temporal bins    
    for time_iter = 1:temporal_bins
        % find the current max and min bins
        first_frame = max([1 round( (time_iter - 1)/temporal_bins * number_of_frames)]);
        last_frame = round(time_iter/temporal_bins * number_of_frames);
        
        % all trajs
        clear trackedPar
        temp_trackedPar = All_trackedPar;    
        trackedPar = struct; 
        count_iter = 1;
        for i=1:length(temp_trackedPar)
            %assign temporal bin 
            curr_frame = temp_trackedPar(i).Frame(1);
            if i >= first_frame && i < last_frame
                trackedPar(count_iter).xy = temp_trackedPar(i).xy;
                trackedPar(count_iter).Frame = temp_trackedPar(i).Frame;
                trackedPar(count_iter).TimeStamp = temp_trackedPar(i).TimeStamp;
                count_iter = count_iter + 1;
            end
        end
        save([time_output_path, Filenames{iter}, '_Tracked_AllTrajs_TimeBin',num2str(time_iter),'.mat'], 'trackedPar', 'settings');
        
        % inside ROI
        clear trackedPar
        temp_trackedPar = in_ROI_trackedPar;    
        trackedPar = struct; 
        count_iter = 1;
        for i=1:length(temp_trackedPar)
            %assign temporal bin 
            curr_frame = temp_trackedPar(i).Frame(1);
            if i >= first_frame && i < last_frame
                trackedPar(count_iter).xy = temp_trackedPar(i).xy;
                trackedPar(count_iter).Frame = temp_trackedPar(i).Frame;
                trackedPar(count_iter).TimeStamp = temp_trackedPar(i).TimeStamp;
                count_iter = count_iter + 1;
            end
        end
        save([time_output_path, Filenames{iter}, '_Tracked_In_ROI_TimeBin',num2str(time_iter),'.mat'], 'trackedPar', 'settings');
    
        
    % outside ROI
        clear trackedPar
        temp_trackedPar = out_ROI_trackedPar;    
        trackedPar = struct; 
        count_iter = 1;
        for i=1:length(temp_trackedPar)
            %assign temporal bin 
            curr_frame = temp_trackedPar(i).Frame(1);
            if i >= first_frame && i < last_frame
                trackedPar(count_iter).xy = temp_trackedPar(i).xy;
                trackedPar(count_iter).Frame = temp_trackedPar(i).Frame;
                trackedPar(count_iter).TimeStamp = temp_trackedPar(i).TimeStamp;
                count_iter = count_iter + 1;
            end
        end
            save([time_output_path, Filenames{iter}, '_Tracked_outside_ROI_TimeBin',num2str(time_iter),'.mat'], 'trackedPar', 'settings');
            
        % inside the Above-ROI control
        clear trackedPar
        temp_trackedPar = above_ROI_trackedPar;    
        trackedPar = struct; 
        count_iter = 1;
        for i=1:length(temp_trackedPar)
            %assign temporal bin 
            curr_frame = temp_trackedPar(i).Frame(1);
            if i >= first_frame && i < last_frame
                trackedPar(count_iter).xy = temp_trackedPar(i).xy;
                trackedPar(count_iter).Frame = temp_trackedPar(i).Frame;
                trackedPar(count_iter).TimeStamp = temp_trackedPar(i).TimeStamp;
                count_iter = count_iter + 1;
            end
        end
        save([time_output_path, Filenames{iter}, '_Tracked_In_Above-ROI_TimeBin',num2str(time_iter),'.mat'], 'trackedPar', 'settings');
        
        % inside the Below-ROI control
        clear trackedPar
        temp_trackedPar = below_ROI_trackedPar;    
        trackedPar = struct; 
        count_iter = 1;
        for i=1:length(temp_trackedPar)
            %assign temporal bin 
            curr_frame = temp_trackedPar(i).Frame(1);
            if i >= first_frame && i < last_frame
                trackedPar(count_iter).xy = temp_trackedPar(i).xy;
                trackedPar(count_iter).Frame = temp_trackedPar(i).Frame;
                trackedPar(count_iter).TimeStamp = temp_trackedPar(i).TimeStamp;
                count_iter = count_iter + 1;
            end
        end
        save([time_output_path, Filenames{iter}, '_Tracked_In_Below-ROI_TimeBin',num2str(time_iter),'.mat'], 'trackedPar', 'settings');
    
    end
      toc;  
        
    
    
    
    clear imgs_3d_matrix imgs_3d_double data_cell_array trackedPar out_ROI_trackedPar in_ROI_trackedPar All_trackedPar
    disp('-----------------------------------------------------------------');
end    

