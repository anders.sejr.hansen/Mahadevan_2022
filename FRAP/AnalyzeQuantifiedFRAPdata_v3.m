%AnalyzeQuantifiedFRAPdata.m
%Anders Sejr Hansen, Jan 2016
clear; clc; close all; 

%   v3, logarithmically spaced timepoints for model fitting
%   n_hist = number of time points for fitting
n_hist = 250;

%Define what to analyze
AnalyzeWhat = 3;   
                     
DoModelFitting = 1;
FirstFitFrame = 21;
ShortenMovie = 1;
LastFrame = 270;


if AnalyzeWhat == 1 % PARP1
    %Workspaces:
    Workspaces = {'6.28.21_FRAP_HaloPARP1_001', '6.28.21_FRAP_HaloPARP1_002', '6.28.21_FRAP_HaloPARP1_003', '6.28.21_FRAP_HaloPARP1_004', '6.28.21_FRAP_HaloPARP1_005', '5.5.21_FRAP_HaloPARP1_002', '5.5.21_FRAP_HaloPARP1_003', '5.5.21_FRAP_HaloPARP1_004','5.5.21_FRAP_HaloPARP1_005','5.5.21_FRAP_HaloPARP1_006', '5.25.21_FRAP_HaloP1_001','5.25.21_FRAP_HaloP1_002', '5.25.21_FRAP_HaloP1_003','5.25.21_FRAP_HaloP1_004', '5.25.21_FRAP_HaloP1_005', '5.25.21_FRAP_HaloP1_006'};
    ToKeep = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
      
elseif AnalyzeWhat == 2 %H2B
    %Workspaces:
    Workspaces = {'6.28.21_FRAP_HaloH2B_001', '6.28.21_FRAP_HaloH2B_002', '6.28.21_FRAP_HaloH2B_003', '6.28.21_FRAP_HaloH2B_004', '6.28.21_FRAP_HaloH2B_005', '5.5.21_FRAP_HaloH2B_001', '5.5.21_FRAP_HaloH2B_002', '5.25.21_FRAP_HaloH2B_001', '5.25.21_FRAP_HaloH2B_002', '5.25.21_FRAP_HaloH2B_003', '5.25.21_FRAP_HaloH2B_004'};
    ToKeep = [1,1,1,1,1,1,1,1,1,1,1];
    
          
elseif AnalyzeWhat == 3 %PARP2
    %Workspaces:
    Workspaces = {'6.28.21_FRAP_HaloPARP2_002', '6.28.21_FRAP_HaloPARP2_003', '6.28.21_FRAP_HaloPARP2_004', '6.28.21_FRAP_HaloPARP2_005', '6.28.21_FRAP_HaloPARP2_006', '6.28.21_FRAP_HaloPARP2_007', '6.28.21_FRAP_HaloPARP2_008', '6.28.21_FRAP_HaloPARP2_011', '6.28.21_FRAP_HaloPARP2_012', '6.28.21_FRAP_HaloPARP2_013', '6.28.21_FRAP_HaloPARP2_014', '6.28.21_FRAP_HaloPARP2_015', '5.5.21_FRAP_HaloPARP2_001', '5.5.21_FRAP_HaloPARP2_002', '5.5.21_FRAP_HaloPARP2_003', '5.5.21_FRAP_HaloPARP2_005', '5.5.21_FRAP_HaloPARP2_006', '5.25.21_FRAP_HaloP2_002', '5.25.21_FRAP_HaloP2_003', '5.25.21_FRAP_HaloP2_004','5.25.21_FRAP_HaloP2_005', '5.25.21_FRAP_HaloP2_006'};
    ToKeep = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,0,0,1];
    
elseif AnalyzeWhat == 4 %NLS
    %Workspaces:
    Workspaces = {'6.28.21_FRAP_HaloNLS_001', '6.28.21_FRAP_HaloNLS_002', '6.28.21_FRAP_HaloNLS_003', '6.28.21_FRAP_HaloNLS_004', '6.28.21_FRAP_HaloNLS_005', '6.28.21_FRAP_HaloNLS_006', '6.28.21_FRAP_HaloNLS_007', '6.28.21_FRAP_HaloNLS_008', '6.28.21_FRAP_HaloNLS_009', '6.28.21_FRAP_HaloNLS_010', '6.28.21_FRAP_HaloNLS_011', '5.5.21_FRAP_HaloNLS_001', '5.25.21_FRAP_HaloNLS_001', '5.25.21_FRAP_HaloNLS_002'};
    ToKeep = [1,1,1,1,1,1,1,1,1,1,1,1,1,1]; 
end

length(Workspaces)
length(ToKeep)
    
    %Load the data
    iter = 1;
    figure('position',[100 100 1400 1000]); %[x y width height]
    for i=1:length(Workspaces)
        if ToKeep(i) >= 1
            load(['./FRAP_workspaces_v2/', Workspaces{i}, '.mat']);
           if iter == 1
                FRAP_data = Init_norm_smallFRAP;
            else
                FRAP_data = vertcat(FRAP_data, Init_norm_smallFRAP);
            end
            iter = iter + 1;
        
        subplot(4,6,i);
        hold on;
        plot(time, Init_norm_smallFRAP, 'ko', 'MarkerSize', 3);
        title([num2str(i), ': FRAP for ', Workspaces{i}], 'FontSize',8, 'FontName', 'Helvetica');
        ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
        xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
        axis([min(time) max(time) 0 1.2]);
        hold off;
        end
    end
    
if ShortenMovie == 1
    FRAP_data = FRAP_data(:,1:LastFrame);
    time = time(1:LastFrame);
end

%PLOT the average data
meanFRAP = mean(FRAP_data, 1);
stdFRAP = std(FRAP_data);
figure('position',[100 100 450 300]); %[x y width height]
hold on;
errorbar(time, meanFRAP, stdFRAP, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 2);
plot(time(FirstBleachFrame:end), smooth(meanFRAP(FirstBleachFrame:end),11), 'k-', 'LineWidth', 3);
title(['FRAP for ', num2str(length(ToKeep)), ' cells'], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(time) max(time) 0 1.2]);
legend('mean with error', 'smoothed mean', 'Location', 'SouthEast');
legend boxoff;
hold off;


if DoModelFitting == 1
    %Do a simply two-exponential fitting:
    %FRAP(t) = 1 - A*exp(-ka*t) - B*exp(-kb*t)
    
    %Variable for fitting:
    xTime1 = time(FirstFitFrame:end)+1;
    yFRAP1 = meanFRAP(FirstFitFrame:end);
    xTime2 = time(FirstBleachFrame:end)+1;
    yFRAP2 = meanFRAP(FirstBleachFrame:end);

    f1 = fittype('1 - A*exp(-ka*x) ');
    [OneExp_fit, OneExp_param] = fit(xTime1', yFRAP1', f1, 'Lower', [0.05 0.0001], 'Upper', [0.9 0.5], 'StartPoint', [0.35 0.025]); 
    OneExp_CI = confint(OneExp_fit);
    
    f2 = fittype('1 - A*exp(-ka*x) - B*exp(-kb*x)');
    [TwoExp_fit, TwoExp_param] = fit(xTime2', yFRAP2', f2, 'Lower', [0.05 0.05 0.05 0], 'Upper', [0.8 4 2 0.05], 'StartPoint', [0.5 1.1 0.5 0.0005]); 
    TwoExp_CI = confint(TwoExp_fit);
    
    xFit1 = min(xTime1):0.25:max(xTime1);
    yFit1 = 1 -  OneExp_fit.A.* exp(-OneExp_fit.ka .* xFit1);
    xFit2 = 0:0.25:max(xTime2);
    yFit2 = 1 - TwoExp_fit.A .* exp(-TwoExp_fit.ka .* xFit2) - TwoExp_fit.B .* exp(-TwoExp_fit.kb .* xFit2);
    
    
    Fit1_text(1) = {'1-Exp fit: 1-A*exp(-ka*t) '};
    Fit1_text(2) = {['A = ', num2str(OneExp_fit.A)]};
    Fit1_text(3) = {['A (95% CI): [', num2str(OneExp_CI(1,1)), ';', num2str(OneExp_CI(2,1)), ']']};
    Fit1_text(4) = {['ka = ', num2str(OneExp_fit.ka), ' s^-1 or 1/k = ', num2str(1/OneExp_fit.ka), 's']};
    Fit1_text(5) = {['a (95% CI): [', num2str(OneExp_CI(1,2)), ';', num2str(OneExp_CI(2,2)), ']']};
    
    
    Fit2_text(1) = {'2-Exp fit: 1-A*exp(-ka*t)-B*exp(-kb*t)'};
    Fit2_text(2) = {['A = ', num2str(TwoExp_fit.A)]};
    Fit2_text(3) = {['A (95% CI): [', num2str(TwoExp_CI(1,1)), ';', num2str(TwoExp_CI(2,1)), ']']};
    Fit2_text(4) = {['ka = ', num2str(TwoExp_fit.ka), ' s^-1 or 1/k = ', num2str(1/TwoExp_fit.ka), 's']};
    Fit2_text(5) = {['a (95% CI): [', num2str(TwoExp_CI(1,3)), ';', num2str(TwoExp_CI(2,3)), ']']};
    Fit2_text(6) = {['B = ', num2str(TwoExp_fit.B)]};
    Fit2_text(7) = {['B (95% CI): [', num2str(TwoExp_CI(1,2)), ';', num2str(TwoExp_CI(2,2)), ']']};
    Fit2_text(8) = {['kb = ', num2str(TwoExp_fit.kb), ' s^-1 or 1/k = ', num2str(1/TwoExp_fit.kb), 's']};
    Fit2_text(9) = {['kb (95% CI): [', num2str(TwoExp_CI(1,4)), ';', num2str(TwoExp_CI(2,4)), ']']};
    
    %Model fitting using logarithmically spaced timepoints
    log_time = logspace(0, log10(time(end)), n_hist);
    %Now find the closest y_value
    log_yFRAP = zeros(1,length(log_time));
    for k = 1:length(log_yFRAP)
        temp_diff = abs(log_time(1,k)-xTime2);
        [val, idx] = min(temp_diff);
        log_yFRAP(1,k) = yFRAP2(1,idx);
    end
    %Now repeat fitting using log-sampled data
    f2_log = fittype('1 - A*exp(-ka*x) - B*exp(-kb*x)');
    [TwoExp_fit_log, TwoExp_param_log] = fit(log_time', log_yFRAP', f2_log, 'Lower', [0 0.05 0 0], 'Upper', [1 10 1 10], 'StartPoint', [0.5 1.1 0.5 0.025]); 
    TwoExp_CI_log = confint(TwoExp_fit_log);
    xFit2_log = 0:0.25:max(xTime2);
    yFit2_log = 1 - TwoExp_fit_log.A .* exp(-TwoExp_fit_log.ka .* xFit2_log) - TwoExp_fit_log.B .* exp(-TwoExp_fit_log.kb .* xFit2_log);
    
    Fit2_text(10) = {'2-Exp fit - log-sampled : 1-A*exp(-ka*t)-B*exp(-kb*t)'};
    Fit2_text(11) = {['A = ', num2str(TwoExp_fit_log.A)]};
    Fit2_text(12) = {['A (95% CI): [', num2str(TwoExp_CI_log(1,1)), ';', num2str(TwoExp_CI_log(2,1)), ']']};
    Fit2_text(13) = {['ka = ', num2str(TwoExp_fit_log.ka), ' s^-1 or 1/k = ', num2str(1/TwoExp_fit_log.ka), 's']};
    Fit2_text(14) = {['a (95% CI): [', num2str(TwoExp_CI_log(1,3)), ';', num2str(TwoExp_CI_log(2,3)), ']']};
    Fit2_text(15) = {['B = ', num2str(TwoExp_fit_log.B)]};
    Fit2_text(16) = {['b (95% CI): [', num2str(TwoExp_CI_log(1,2)), ';', num2str(TwoExp_CI_log(2,2)), ']']};
    Fit2_text(17) = {['ka = ', num2str(TwoExp_fit_log.kb), ' s^-1 or 1/k = ', num2str(1/TwoExp_fit_log.kb), 's']};
    Fit2_text(18) = {['a (95% CI): [', num2str(TwoExp_CI_log(1,4)), ';', num2str(TwoExp_CI_log(2,4)), ']']};
    
    
    %%% PLOT THE FITS TO THE DATA %%%
    figure('position',[100 100 450 300]); %[x y width height]
    hold on;
    plot(time, meanFRAP, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
    plot(xFit1, yFit1, 'k--', 'LineWidth', 3);
    plot(xFit2, yFit2, 'k-', 'LineWidth', 2);
    plot(xFit2_log, yFit2_log, 'k-.', 'LineWidth', 2);
    plot([min(time), max(time)], [1, 1], 'k--', 'LineWidth', 1);
    text(10,0.3,Fit1_text,'HorizontalAlignment','Left', 'FontSize',9, 'FontName', 'Helvetica');
    text(140,0.6,Fit2_text,'HorizontalAlignment','Left', 'FontSize',9, 'FontName', 'Helvetica');
    title('Exponential fit to PARP2 JF646 FRAP', 'FontSize',10, 'FontName', 'Helvetica');
    ylabel('FRAP recovery', 'FontSize',10, 'FontName', 'Helvetica');
    xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
    axis([min(time) max(time) 0 1.08]);
    legend('FRAP Data', '1-Exp model fit', '2-Exp model fit', 'log sample 2Exp fit', 'Location', 'SouthEast');
    legend boxoff;
    hold off;

end






