%SegmentQuantifyMovie_v2.m
%Anders Sejr Hansen, Jan 2016
clear; clc; close all;

%The purpose of this movie is to analyze FRAP data and quantify the
%intensity of a circular bleached region that is moving a little bit, using
%just manual adjustment of the circle movement. 
%imshow(images(:,:,1), []);

%VERSION 2 - SEGMENT THE WHOLE NUCLEUS
%Use inital fluorescence in the bleach spot region to normalize the final
%recovery - use a slightly larger circle to correct for noise. Use an extra
%radius of:
ExtraRadius = 10;
%Adjust the intensity threshold used across the movie to account for
%photobleaching. Assume ~18% photobleach over the 300 frames.
ExpFactor = 3.5; %exp(-endFrame/(ExpFactor*endFrame)) = 0.8187

%Use WhatToDo = 1 to find BleachCentre. Use WhatToDo = 2 to quantify BleachCentre Movement
WhatToDo = 2;
SaveVal = 0; %Change to 1 if you want to save

% Muller, Wach, McNally, BioPhys J, 2008: Can ignore Gaussian shape of photobleach spot when the recovery is slow. 

SampleName = '5.5.21_FRAP_HaloH2B_003';
file = '5.5.21_FRAP_HaloH2B_003';
path = [pwd, filesep, 'RawData', filesep];
MakeMovie = 0; 

%Define the FRAP circle:
FirstBleachFrame = 21;
FrameRate = 1; 
CircleRadius = 10; 
SmallCircleRadius = 6;
NewTime = 0;
%Define FRAP Cirlce movement
FrameUpdate = 10; %Update every 10th frame

%For some of the U2-OS movies, the background was hidden. Need to adjust
%the background manually (Otherwise FRAP recovery is negative in first
%time-point). Use:
U2OS_background = 0;


%SAMPLE SPECIFIC INFORMATION
if strcmp(SampleName, '5.5.21_FRAP_HaloH2B_001');
    disp(SampleName);
    IntThres = 250; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [11, 11];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
 
elseif strcmp(SampleName, '5.5.21_FRAP_HaloH2B_002');
    disp(SampleName);
    IntThres = 250; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [11, 11];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
        
        
elseif strcmp(SampleName, '5.5.21_FRAP_HaloPARP1_001');
    disp(SampleName);
    IntThres = 60; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [11, 11];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
        
        
elseif strcmp(SampleName, '5.5.21_FRAP_HaloPARP1_002');
    disp(SampleName);
    IntThres = 60; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [11, 11];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
 
        
elseif strcmp(SampleName, '5.5.21_FRAP_HaloPARP1_003');
    disp(SampleName);
    IntThres = 60; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [241, 11];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
        
elseif strcmp(SampleName, '5.5.21_FRAP_HaloPARP1_004');
    disp(SampleName);
    IntThres = 60; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [11, 11];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
        
elseif strcmp(SampleName, '5.5.21_FRAP_HaloH2B_003');
    disp(SampleName);
    IntThres = 250; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [11, 11];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; ...
            0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
        
else
    error('SampleName was not found. Please check your SampleName again');
end 
%%%%%%%%%%%%%% LOAD THE DATA %%%%%%%%%%%%%


% FIND JYOTHI'S BLEACH CENTRE:
bleach_ROI = imread([path, file, '_ROI.tif']); %binary mask with bleach ROI
% find the centre:
temp = regionprops(bleach_ROI,'centroid');
BleachCentre = temp.Centroid;

%Open the file using the BioFormats package
%temp = bfopen([path, file]);
[stack, nbImages] = tiffread([path, file, '.tif']);

%Convert to a matrix
%images = zeros(size(data{1,1},1), size(data{1,1},2), size(data,1));
images = zeros(stack(1,1).height, stack(1,1).width, size(stack,2));
ImHeight = stack(1,1).height; ImWidth = stack(1,1).width; 
for i=1:size(images,3)
    images(:,:,i) = stack(1,i).data;
end
if NewTime == 0
    time = -(FirstBleachFrame-1)/FrameRate:(size(images, 3)-FirstBleachFrame)/FrameRate;
elseif NewTime == 1 %mRAD21 JF646 movies start at 10 s
    time = [-(FirstBleachFrame-2)/FrameRate:0 10:(size(images, 3)-FirstBleachFrame+10)/FrameRate];
elseif NewTime == 2
    % took a frame every 2 seconds:
    time = -(FirstBleachFrame-1)/FrameRate:(size(images, 3)-FirstBleachFrame)/FrameRate;
    time = time .* 2;
end

%Make smoothed images for segmentation of the nucleus
%Perform gaussian smoothing
smooth_filter = fspecial('gaussian', [6 6], 2);
smooth_images = zeros(stack(1,1).height, stack(1,1).width, size(stack,2));
for i=1:size(images,3)
    smooth_images(:,:,i) = imfilter(images(:,:,i), smooth_filter);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%WhatToDo = 1: determine bleach region
if WhatToDo == 1
    %Define the circle:
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    BW_circle = sqrt((rr-BleachCentre(1,1)).^2+(cc-BleachCentre(1,2)).^2)<=CircleRadius;
    %imshow(BW_circle) 
    figure('position',[10 1000 700 250]); %[x y width height]
    subplot(1,3,1);
    imshow(images(:,:,FirstBleachFrame), []);
    
    subplot(1,3,2);
    overlay1 = imoverlay(mat2gray(images(:,:,FirstBleachFrame)), bwperim(BW_circle), [.3 1 .3]);
    imagesc(overlay1);
    title('Matching a circle to the region');
    
    %Make circles for background
    %Threshold to identify the nucleus
    Nucleus_binary = smooth_images(:,:,FirstBleachFrame) > IntThres*exp(-FirstBleachFrame/(ExpFactor*length(time)));
    %Now fill in any small holes in the middle of the nucleus:
    Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
    subplot(1,3,3);
    overlay2 = imoverlay(mat2gray(images(:,:,FirstBleachFrame)), bwperim(Nucleus_binary_filled), [1 0 0]);
    imagesc(overlay2);
    title('Nucleus segmentation');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%WhatToDo = 2: Determine Bleach Movement
if WhatToDo == 2
    FRAP_circle = zeros(1, round(size(images,3)/FrameUpdate));
    %Figure out how much the bleach circle moved
    for n=1:length(FRAP_circle)
        CumMove = Movement(1:n,:); %The cumulative movement of the circle
        CurrCentre = BleachCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
        currBackgroundCentre = BackgroundCentre;
        
        %Update the circle
        [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
        BW_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius;

        
        %Find the pixels inside the circle:
        [row,col,val] = find(BW_circle); clear val
        FRAP_vals = zeros(1,length(row));
        for i=1:length(row)
            FRAP_vals(1,i) = images(row(i), col(i), n*FrameUpdate);
        end
        FRAP_circle(1,n) = mean(FRAP_vals);
        %Plot the circle fit
        figure('position',[100 100 500 250]); %[x y width height]
        subplot(1,2,1);
        overlay1 = imoverlay(mat2gray(images(:,:,n*FrameUpdate)), bwperim(BW_circle), [.3 1 .3]);
        imagesc(overlay1);
        title(['FRAP spot in frame ', num2str(n*FrameUpdate)]);
        
        %Threshold to identify the nucleus
        Nucleus_binary = smooth_images(:,:,n*FrameUpdate) > IntThres*exp(-n*FrameUpdate/(ExpFactor*length(time)));
        %Now fill in any small holes in the middle of the nucleus:
        Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
        
        subplot(1,2,2);
        overlay2 = imoverlay(mat2gray(images(:,:,n*FrameUpdate)), bwperim(Nucleus_binary_filled), [1 0 0]);
        imagesc(overlay2);
        title(['Segmented nucleus in frame ', num2str(n*FrameUpdate)]);
        
        print('-dpng',['./images/', file, '_FRAME_', num2str(n*FrameUpdate), '.png' ]);
        close;
    end
    figure; 
    hold on;
    plot(1:length(FRAP_circle), FRAP_circle);
    
    hold off;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DETERMINE THE INITIAL FLUORESCENCE IN THE BLEACH AREA %%%
InitSpotIntensity = zeros(1,FirstBleachFrame-1);
for n = 1:FirstBleachFrame-1
    CumMove = Movement(1:max([round(n/FrameUpdate) 1]),:); %The cumulative movement of the circle
    CurrCentre = BleachCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    %Update the circle
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    InitSpot_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius+ExtraRadius;
    %Find pixels
    [row,col,val] = find(InitSpot_circle); clear val
    InitSpot_vals = zeros(1,length(row));
    for i=1:length(row)        
        InitSpot_vals(1,i) = images(row(i), col(i), n);
    end
    InitSpotIntensity(1,n) = mean(InitSpot_vals);
end

%%%%%% PLOT FRAP CURVES %%%%
FRAP_intensity = zeros(1, size(images, 3));
SmallCircle_intensity = zeros(1, size(images, 3));
Nuc_intensity = zeros(1, size(images, 3)); 
Black_intensity = zeros(1, size(images, 3));
for n = 1:size(images, 3)
    CumMove = Movement(1:max([round(n/FrameUpdate) 1]),:); %The cumulative movement of the circle
    CurrCentre = BleachCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    %currBackgroundCentre = BackgroundCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    currBackgroundCentre = BackgroundCentre;
    
    %Update the circle
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    BW_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius;
    Small_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=SmallCircleRadius;
    Black_circle = sqrt((rr-BackgroundCentre(1,1)).^2+(cc-BackgroundCentre(1,2)).^2)<=CircleRadius;
    %Find the pixels inside the circle:
    [row,col,val] = find(BW_circle); clear val
    FRAP_vals = zeros(1,length(row));
    for i=1:length(row)        
        FRAP_vals(1,i) = images(row(i), col(i), n);
    end
    FRAP_intensity(1,n) = mean(FRAP_vals);
    
    [row2,col2,val] = find(Small_circle); clear val
    Small_vals = zeros(1,length(row2));
    for i=1:length(row2)        
        Small_vals(1,i) = images(row2(i), col2(i), n);
    end
    SmallCircle_intensity(1,n) = mean(Small_vals);
    
    %SEGMENT THE NUCLEUS
    %Threshold to identify the nucleus
    Nucleus_binary = smooth_images(:,:,n) > IntThres*exp(-n/(ExpFactor*length(time)));
    %Now fill in any small holes in the middle of the nucleus:
    Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
    
    
    [nuc_row,nuc_col,val] = find(Nucleus_binary_filled); clear val
    [row4,col4,val] = find(Black_circle); clear val
    nuc_vals = zeros(1,length(nuc_row)); 
    Black_vals = zeros(1,length(row4));
    for i=1:length(nuc_row)        
        nuc_vals(1,i) = images(nuc_row(i), nuc_col(i), n);
    end
    for i=1:length(row4)        
        Black_vals(1,i) = images(row4(i), col4(i), n);
    end
    Nuc_intensity(1,n) = mean(nuc_vals);
    if U2OS_background > 0
        Black_intensity(1,n) = mean(Black_vals) - U2OS_background; 
    else
        Black_intensity(1,n) = mean(Black_vals);
    end
end
%PLOT the results
figure('position',[10 600 800 450]); %[x y width height]
subplot(1,3,1);
hold on;
plot(time, FRAP_intensity, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, SmallCircle_intensity, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);
plot(time, Black_intensity, 'ko', 'MarkerSize', 6);
plot(time, Nuc_intensity, 'o', 'Color', [100/255, 151/255, 75/255] , 'MarkerSize', 6);

plot(time(FirstBleachFrame:end), FRAP_intensity(FirstBleachFrame:end), '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 1);
plot(time(FirstBleachFrame:end), SmallCircle_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Black_intensity(FirstBleachFrame:end), 'k-', 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back1_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back2_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back3_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
legend('FRAP: r=10', 'FRAP: r=6', 'Black bg', 'whole Nuc', 'Location', 'NorthEast');
axis([min(time) max(time) 0 1.55*max(FRAP_intensity)]);
title('Raw FRAP quantification', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('Halo-mCTCF TMR fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%% PERFORM ANALYSIS AND PHOTOBLEACHING CORRECTION %%%%%%%%%%%%%%%%

%Rationale for the analysis: first of all, subtract the black background.
%Second, use the total nuclear fluorescence to correct for
%photobleaching and reduction in cell fluorescence overall. Furthermore, the cell 
%moves a bit so it is important to take into account that cell total and
%local fluorescence can change a bit. In some of the movies, cell moves
%quite a bit so this is important to do. 

%So do the following:
%1. subtract black background from all. 
%2. Divide the FRAP circle by the average nuclear background throughout. 

%1. SUBTRACT BLACK BACKGROUND
bsFRAP_intensity = FRAP_intensity - Black_intensity;
bsSmallCircle_intensity = SmallCircle_intensity - Black_intensity;
bsNuc_intensity = Nuc_intensity - Black_intensity;
InitSpotIntensity = InitSpotIntensity - Black_intensity(1:FirstBleachFrame-1);

%3. ADJUST FOR PHOTOBLEACHING
%adjust_FRAP = [bsFRAP_intensity(1:FirstBleachFrame-1)./ bsNuc_intensity(1:FirstBleachFrame-1) bsFRAP_intensity(FirstBleachFrame)/mean(bsNuc_intensity(1:FirstBleachFrame-1)) bsFRAP_intensity(FirstBleachFrame+1:end)./ bsNuc_intensity(FirstBleachFrame+1:end)];
%adjust_smallFRAP = [bsSmallCircle_intensity(1:FirstBleachFrame-1)./ bsNuc_intensity(1:FirstBleachFrame-1) bsSmallCircle_intensity(FirstBleachFrame)/mean(bsNuc_intensity(1:FirstBleachFrame-1)) bsSmallCircle_intensity(FirstBleachFrame+1:end)./ bsNuc_intensity(FirstBleachFrame+1:end)];
InitSpotIntensity = InitSpotIntensity./(bsNuc_intensity(1:FirstBleachFrame-1));

adjust_FRAP = bsFRAP_intensity ./smooth(bsNuc_intensity, 3)';
adjust_smallFRAP = bsSmallCircle_intensity ./smooth(bsNuc_intensity, 3)';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%% PLOT THE ADJUSTED FRAP CURVES %%%%%%%%%%%%%%%%%%%%%%%
subplot(1,3,2);
hold on;
plot(time, adjust_FRAP, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, adjust_smallFRAP, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);

plot(time(FirstBleachFrame:end), smooth(adjust_FRAP(FirstBleachFrame:end),7)', '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 2);
plot(time(FirstBleachFrame:end), smooth(adjust_smallFRAP(FirstBleachFrame:end),7)', '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 2);
legend('FRAP: r=10', 'FRAP: r=6',  'Location', 'NorthEast');
axis([min(time) max(time) 0 1.05*max(adjust_smallFRAP)]);
title('nuclear normalized FRAP', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('relative Halo-mCTCF TMR fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;


%%%%%%%%%%%%%%%%%%%%% NORMALIZE BASED ON INITIAL FLUORESCENCE %%%%%%%%%%%%%%%%%%%%%%%
Init_norm_FRAP = adjust_FRAP./mean(InitSpotIntensity);
Init_norm_smallFRAP = adjust_smallFRAP./mean(InitSpotIntensity);

subplot(1,3,3);
hold on;
plot(time, Init_norm_FRAP, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, Init_norm_smallFRAP, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);
plot([min(time) max(time)], [1 1], 'k--', 'LineWidth', 1);
plot(time(FirstBleachFrame:end), smooth(Init_norm_FRAP(FirstBleachFrame:end),7)', '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 2);
plot(time(FirstBleachFrame:end), smooth(Init_norm_smallFRAP(FirstBleachFrame:end),7)', '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 2);
legend('FRAP: r=10', 'FRAP: r=6',  'Location', 'NorthEast');
axis([min(time) max(time) 0 1.15]);
title('Initial spot normalized FRAP', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('relative Halo-mCTCF TMR fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;



%%%%%%%%%%%%%%%%%%%%%%%%%% SAVE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if SaveVal == 1;
    clearvars -except time FirstBleachFrame BleachCentre FRAP_intensity SmallCircle_intensity BackgroundCentre Movement SampleName adjust_FRAP adjust_smallFRAP bsNuc_intensity Init_norm_FRAP Init_norm_smallFRAP ExtraRadius ExpFactor
    
    save(['./FRAP_workspaces_v2/', SampleName, '.mat']);
    MakeMovie = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%% MAKE A MOVIE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if MakeMovie == 1
    %Adjust CumMove:
    FullCumMove = zeros(length(time), 2);
    for i=1:size(CumMove,1)
        CurrIndex = FrameUpdate + (i-1)*FrameUpdate;
        FullCumMove(CurrIndex,:) = CumMove(i,:);
    end
    prebleach = mean(adjust_smallFRAP(1:FirstBleachFrame-1));
    adjust_smallFRAP2 = [adjust_smallFRAP(1:FirstBleachFrame-1)./prebleach adjust_smallFRAP(FirstBleachFrame:end)];
           
    %Figure out how much the bleach circle moved
    for n=1:size(FullCumMove,1)
        %Update the FRAP circle centre
        CurrCentre = BleachCentre - [sum(FullCumMove(1:n,1)), sum(FullCumMove(1:n,2))];

        %Update the circle
        [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
        BW_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius;
        
        %Plot the circle fit
        figure('position',[100 100 650 250]); %[x y width height]
        subplot(1,2,1);
        overlay1 = imoverlay(mat2gray(images(:,:,n)), bwperim(BW_circle), [.3 1 .3]);
        imagesc(overlay1);
        title(['Halo-mCTCF FRAP image at ', num2str(n-FirstBleachFrame), ' seconds']);
        set(gca,'XTick',[]); set(gca,'YTick',[]);
        
        subplot(1,2,2);
        hold on;
        plot(time(1:n), Init_norm_smallFRAP(1:n), 'o', 'Color', [237/255, 28/255, 36/255] , 'MarkerSize', 6);
        axis([min(time) max(time) 0 1.15]);
        title('Quanfied FRAP recovery', 'FontSize',10, 'FontName', 'Helvetica');
        ylabel('relative TMR fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
        xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
        hold off;
        print('-dpng', '-r300',['./images/', file, '_FRAP_Movie_', num2str(n), '.png' ]);
        close;
    end
end