%PLOT_MultipleFRAPcurves.m
%Anders Sejr Hansen, Apr 2016
clear; clc; %close all; 

%Do you want to include a 2-Exp fit (set = 1 if yes):
IncludeFit = 1;

%Plot multiple FRAP curves on the same plot
%What to include:
    
Include = [1,1];
colours = [0/255, 153/255, 204/255; 237/255, 28/255, 36/255; 0/255, 161/255, 75/255; 0,0,0; 102/255, 45/255, 145/255; 157/255, 157/255, 54/255];

%Make a structured array for storing all of the data
Struc_data = struct([]);
Struc_data(1).Workspaces = {'5.5.21_FRAP_HaloPARP1_002', '5.5.21_FRAP_HaloPARP1_003', '5.5.21_FRAP_HaloPARP1_004'};
Struc_data(1).ToKeep = [1,1,1];
Struc_data(1).name = 'Halo-PARP1';
Struc_data(2).Workspaces = {'5.5.21_FRAP_HaloH2B_001', '5.5.21_FRAP_HaloH2B_002'};
Struc_data(2).ToKeep = [1,1];
Struc_data(2).name = 'H2b-Halo';

    
Replicate_data = struct([]); cell_iter = 1;
%Compile the FRAP curve for each workspace
for i=1:length(Struc_data)
    if Include(i) == 1
        
        %Load in the data
        iter = 1;
        replicate = 1;
        for j=1:length(Struc_data(i).ToKeep)
            if Struc_data(i).ToKeep(j) >= 1
                load(['./FRAP_workspaces_v2/', Struc_data(i).Workspaces{j}, '.mat']);
                Init_norm_smallFRAP(1:FirstBleachFrame-1) = Init_norm_smallFRAP(1:FirstBleachFrame-1)./mean(Init_norm_smallFRAP(1:FirstBleachFrame-1));
                if Struc_data(i).ToKeep(j) ==  replicate
                    Replicate_data(cell_iter,replicate).FRAP(iter,:) = Init_norm_smallFRAP;
                else
                    replicate = Struc_data(i).ToKeep(j);
                    iter = 1;
                    Replicate_data(cell_iter,replicate).FRAP(iter,:) = Init_norm_smallFRAP;
                end
                iter = iter + 1;
                
            end
        end
        
        %Save the FRAP curve for that experiment
        Struc_data(i).time = time;
        cell_iter = cell_iter + 1;
    end        
end 


%Calculate the mean and STD among the replicates:
FRAP_means = zeros(size(Replicate_data,1), length(time));
FRAP_stds = zeros(size(Replicate_data,1), length(time));
for i=1:size(Replicate_data,1)
    rep_FRAP_mean = [];
    iter = 1;
    curr_FRAP = Replicate_data(i,1).FRAP;
    for j=1:size(curr_FRAP,1)
        rep_FRAP_mean(iter,:) = curr_FRAP(j,:);
        iter = iter + 1;

    end
    FRAP_means(i,:) = mean(rep_FRAP_mean,1);
    FRAP_stds(i,:) = std(rep_FRAP_mean,1);
end
%PLOT THE OUTPUT
figure('position',[100 100 450 300]); %[x y width height]
LegendNames = {};
hold on;
iter = 1;
for i=1:length(Struc_data)
    if Include(i) == 1
        
        %Do Double-exponential fit for trend curve
        %Variable for fitting:
        if IncludeFit == 1
        xTime = Struc_data(i).time(FirstBleachFrame:end)+1;
        yFRAP = FRAP_means(iter,FirstBleachFrame:end);
        f2 = fittype('1 - A*exp(-ka*x) - B*exp(-kb*x)');
        [TwoExp_fit, TwoExp_param] = fit(xTime', yFRAP', f2, 'Lower', [0 0.05 0 0], 'Upper', [0.95 10 2 10], 'StartPoint', [0.5 1.1 0.5 0.025]); 
        %TwoExp_CI = confint(TwoExp_fit);
        xFit = min(xTime)-1:0.25:max(xTime);
        yFit = 1 - TwoExp_fit.A .* exp(-TwoExp_fit.ka .* xFit) - TwoExp_fit.B .* exp(-TwoExp_fit.kb .* xFit);
        end
        
        %perform actual PLOTTING
        
        %Set plot properties
        lineProps.col{1} = colours(iter,:);
        %lineProps.style = '-';
        %lineProps.width = 2;
        lineProps.style = 'o';
        lineProps.width = 1;
        
        %H = mseb(Struc_data(i).time(1:FirstBleachFrame-1), smooth(FRAP_means(iter,1:FirstBleachFrame-1))', smooth(FRAP_stds(iter,1:FirstBleachFrame-1))', lineProps, 0);
        %set(get(get(H(1).mainLine,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        %mseb(Struc_data(i).time(FirstBleachFrame:end), smooth(FRAP_means(iter,FirstBleachFrame:end),3)', smooth(FRAP_stds(iter,FirstBleachFrame:end),3)', lineProps, 0);
        errorbar(Struc_data(i).time, FRAP_means(iter,:), FRAP_stds(iter,:), 'o', 'Color', colours(iter,:), 'MarkerSize', 6);
        if IncludeFit == 1
        h = plot(xFit, yFit, 'Color', colours(iter,:), 'LineWidth', 2);
        set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        end
        LegendNames{iter} = Struc_data(i).name;

        iter = iter + 1;
    end
end
title('FRAP dynamics', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('FRAP recovery', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(time) max(time) 0 1.08]);
legend(LegendNames, 'Location', 'SouthEast');
legend boxoff;
hold off;



